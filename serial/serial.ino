#include <SPI.h>
#include <mcp2515.h>
#include <string.h>

struct can_frame canMsg;
struct can_frame canMsg1;
struct can_frame canMsg2;
MCP2515 mcp2515(53);

void setup()
{

  canMsg1.can_id = 0x008;
  canMsg1.can_dlc = 8;
  __u8 a[8] = {0x00, 0xDA, 0x00, 0x19, 0x00, 0x00, 0x00, 0x3F};

  canMsg2.can_id = 0x4;
  canMsg2.can_dlc = 8;
  canMsg2.data[0] = 0x00;
  canMsg2.data[1] = 0xDA;
  canMsg2.data[2] = 0x00;
  canMsg2.data[3] = 0x12;
  canMsg2.data[4] = 0x00;
  canMsg2.data[5] = 0x00;
  canMsg2.data[6] = 0xFF;
  canMsg2.data[7] = 0x0F;

  while (!Serial);
  Serial.begin(115200);
  mcp2515.reset();
  mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ);
  // mcp2515.setLoopbackMode();
  mcp2515.setNormalMode();
  Serial.print(" Ready ;");
}
void loop()
{
  // mcp2515.sendMessage(&canMsg1);
  // mcp2515.sendMessage(&canMsg1);
  // //Serial.println("Messages sent");
  // mcp2515.sendMessage(&canMsg2);
  // delay(100);
  // if (mcp2515.readMessage(&canMsg2) == MCP2515::ERROR_OK)
  // {
  //   Serial.print(canMsg2.can_id, HEX); // print ID
  //   Serial.print(" ");
  //   Serial.print(canMsg2.can_dlc, HEX); // print DLC

  //   for (int i = 0; i < canMsg2.can_dlc; i++)  {
  //     Serial.print(" ");
  //     Serial.print(canMsg2.data[i],HEX);
  //   }
  //   Serial.print(';');
  // }
  if (mcp2515.readMessage(&canMsg1) == MCP2515::ERROR_OK)
  {
    Serial.print(canMsg1.can_id, HEX); // print ID
    Serial.print(" ");
    Serial.print(canMsg1.can_dlc, HEX); // print DLC

    for (int i = 0; i < canMsg1.can_dlc; i++)
    {
      Serial.print(" ");
      Serial.print(canMsg1.data[i], HEX);
    }
    Serial.print(';');
  }

  if (Serial.available())
  {
    String msg = Serial.readStringUntil(';');
    canMsg2.can_id  = (char)msg[0];
    canMsg2.can_dlc = (char)msg[1];
    canMsg2.data[0] = (char)msg[2];
    canMsg2.data[1] = (char)msg[3];
    canMsg2.data[2] = (char)msg[4];
    canMsg2.data[3] = (char)msg[5];
    canMsg2.data[4] = (char)msg[6];
    canMsg2.data[5] = (char)msg[7];
    canMsg2.data[6] = (char)msg[8];
    canMsg2.data[7] = (char)msg[9];
    mcp2515.sendMessage(&canMsg2);
   
    delay(100);
    if (mcp2515.readMessage(&canMsg1) == MCP2515::ERROR_OK)
    {
      Serial.print(canMsg1.can_id, HEX); // print ID
      Serial.print(" ");
      Serial.print(canMsg1.can_dlc, HEX); // print DLC

      for (int i = 0; i < canMsg1.can_dlc; i++)
      {
        Serial.print(" ");
        Serial.print(canMsg1.data[i], HEX);
      }
      Serial.print(';');
    }
  }
}