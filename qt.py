import sys
import serial
import serial.tools.list_ports
import warnings
from PyQt5.QtCore import QSize, QRect, QObject, pyqtSignal, QThread, pyqtSignal, pyqtSlot
import time
from PyQt5.QtWidgets import QHeaderView, QApplication, QComboBox, QDialog, QMainWindow, QWidget, QLabel, QTextEdit, QListWidget, QListView
from PyQt5.uic import loadUi

ports = [
    p.device
    for p in serial.tools.list_ports.comports()
    # if 'USB' in p.description
]

# if not ports:
#     raise IOError("Seri Baglantili cihaz yok!")

if len(ports) > 1:
    warnings.warn('chose the com port....')

ser = serial.Serial()
ser.close()
# class Foo(QThread):
#    def connect(self):
#        connect(bt_stop, SIGNAL("clicked()"), on_bt_stop_clicked)

# MULTI-THREADING


class Worker(QObject):
    finished = pyqtSignal()
    intReady = pyqtSignal(str)

    @pyqtSlot()
    def __init__(self):
        super(Worker, self).__init__()
        self.working = True

    def work(self):
        while self.working:
            if ser.in_waiting > 5:
                line = ser.read_until(bytes(';', 'utf-8')).decode()
                print(line)
                time.sleep(0.01)
                self.intReady.emit(line)
        self.finished.emit()


class qt(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        loadUi('qt.ui', self)
        # set default value
        self.cb_baud.setCurrentIndex(4)  # baud default value = 9600
        self.cb_timeout.setCurrentIndex(1)  # time out default value = 0
        self.cb_size.setCurrentIndex(2)
        # end set default value for serial
        # self.cb_timeout.activated.connect(self.load_valTimeout)
        # self.cb_baud.activated.connect(self.load_valBaud)
        # prepare for multi thread
        self.thread = None
        self.worker = None
        self.bt_start.clicked.connect(self.start_loop)
        self.bt_send.clicked.connect(self.bt_send_clicked_cac)
        #set up for the table
        table =  self.RStable.horizontalHeader()
        table.setSectionResizeMode(2, QHeaderView.Stretch)
        # model = self.RStable.model()
        self.bt_sendList.clicked.connect(self.a)
        if(ser.is_open):
            self.bt_send.setEnabled(True)
            self.bt_sendList.setEnabled(True)
            self.bt_start.setEnabled(False)
        else:
            self.bt_send.setEnabled(False)
            self.bt_sendList.setEnabled(False)
            self.bt_start.setEnabled(True)


    def loop_finished(self):
        print('Looped Finished')

    def start_loop(self):
        if not (ser.is_open):
            ser.open()
            self.bt_send.setEnabled(True)
            self.bt_sendList.setEnabled(True)
        self.bt_start.setEnabled(False)
        self.worker = Worker()  # a new worker to perform those tasks
        self.thread = QThread()  # a new thread to run our background tasks in
        # move the worker into the thread, do this first before connecting the signals
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.work)
        # begin our worker object's loop when the thread starts running
        self.worker.intReady.connect(self.onIntReady)
        # stop the loop on the stop button click
        self.bt_stop.clicked.connect(self.stop_loop)
        # do something in the gui when the worker loop ends
        self.worker.finished.connect(self.loop_finished)
        # tell the thread it's time to stop running
        self.worker.finished.connect(self.thread.quit)
        # have worker mark itself for deletion
        self.worker.finished.connect(self.worker.deleteLater)
        # have thread mark itself for deletion
        self.thread.finished.connect(self.thread.deleteLater)
        # make sure those last two are connected to themselves or you will get random crashes
        self.thread.start()

    def stop_loop(self):
        self.worker.working = False
        # self.thread.quit
        # self.worker.deleteLater
        # self.thread.deleteLater
        self.label_5.setText("DISCONNECTED!")
        self.label_5.setStyleSheet('color: red')
        ser.close()
        self.bt_send.setEnabled(False)
        self.bt_start.setEnabled(True)
        self.bt_sendList.setEnabled(False)

    def onIntReady(self, i):
        # data_split = i.split(' ')
        # dis = int(data_split[4],16)*1000 + int(data_split[5],16)*100 +int(data_split[6],16)*10 +int(data_split[7],16)
        disText = "<span style=\" font-size:8pt; font-weight:600; color:#ff0000;\" >"
        disText = disText + i + "</span>"
        self.te_serial.append("{}".format(disText))

    # def load_valTimeout(self, index0):
    #     self.a2 = self.cb_timeout.itemText(index0)
    #     print(self.cb_timeout.itemText(index0))
    #     self.x = 2

    # def load_valBaud(self, index1): # baudrate
    #     print(self.cb_baud.currentText())
    #     self.x = 1

    def on_bt_confirm_clicked(self):
        if self.x != 0:
            self.te_note.setText('Save config!')
        else:
            self.te_note.setText('Please choose COM and baudrate!')

    def on_bt_log_clicked(self):
        with open('log.txt', 'w') as f:
            my_text = self.te_serial.toPlainText()
            f.write(my_text)

    def on_bt_stop_clicked(self):
        self.te_note.setText('Stopped! Press START to reconnect')

    def on_bt_start_clicked(self):
        ser.port = ports[0]
        ser.baudrate = int(self.cb_baud.currentText())
        self.label_11.setText(ports[0])
        # self.completed = 0
        # while self.completed < 100:
        #     self.completed += 0.001
        #     self.progressBar.setValue(int(self.completed))
        self.te_note.setText('Receiving Data ...')
        self.label_5.setText("CONNECTED!")
        self.label_5.setStyleSheet('color: green')
        # x = 1
        self.te_serial.setText("start!")

    def bt_send_clicked_cac(self):
        # sd = []
        a = self.te_id.toPlainText()
        b = self.te_dlc.toPlainText()
        c = self.textEdit_2.toPlainText()
        self.send_data(a, b, c)
        # sd.append(int(id, 16))
        # sd.append(int(dlc, 16))
        # sd.append(int(data[0],16))
        # sd.append(int(data[1],16))
        # sd.append(int(data[2],16))
        # sd.append(int(data[3],16))
        # sd.append(int(data[4],16))
        # sd.append(int(data[5],16))
        # sd.append(int(data[6],16))
        # sd.append(int(data[7],16))
        # sd.append(59)
        # ser.write(bytearray(sd))
        # ser.write(mytext.encode())

    def send_data(self, id, dlc, data_str):
        sd = []
        data = data_str.split(' ')
        sd.append(int(id, 16))
        sd.append(int(dlc, 16))
        sd.append(int(data[0], 16))
        sd.append(int(data[1], 16))
        sd.append(int(data[2], 16))
        sd.append(int(data[3], 16))
        sd.append(int(data[4], 16))
        sd.append(int(data[5], 16))
        sd.append(int(data[6], 16))
        sd.append(int(data[7], 16))
        sd.append(59)
        self.te_serial.append(id + " " + dlc + " " + data_str)
        ser.write(bytearray(sd))
    
    def a(self):
        i = 0
        print("send list")
        model = self.RStable.model()
        # model.data(model.index(i,0)) == None and model.data(model.index(i,1)) == None and model.data(model.index(i,2)) == None
        while(model.data(model.index(i,0)) != None and model.data(model.index(i,0)) != ''):
            print("send")
            id = model.data(model.index(i,0))
            dlc = model.data(model.index(i,1))
            data = model.data(model.index(i,2))
            self.send_data(id, dlc, data)
            i += 1
            print(i)
        print("send list done")


def run():
    app = QApplication(sys.argv)
    widget = qt()
    widget.show()
    sys.exit(app.exec_())


def main():
    # run()
    id = '7'
    dlc = '8'
    data = '00 da 00 17 00 00 00 1f'
    data_send = []
    try:
        data_send.append(int(id, 16))
        data_send.append(int(dlc, 16))
        for i in data.split(' '):
            data_send.append(int(i, 16))
    except:
        print("id, dlc or data is not hex")
    data_send.append(ord(';'))
    while(True):
        print("vl")
        line = ser.readline()
        print(line)
        # ser.write(bytearray(data_send))
        time.sleep(0.1)


if __name__ == "__main__":
    run()
